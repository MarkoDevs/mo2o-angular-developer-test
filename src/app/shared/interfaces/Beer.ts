export interface Beer{
  id : number,
  name : string,
  tagline : string,
  first_brewed : Date,
  description : string,
  image_url : URL,
  abv : number,
  ibu : number,
  target_fg : number,
  target_og: number,
  ebc: number,
  srm : number,
  ph : number,
  attenuation_level : number,
  volume: Measure,
  boil_volume : Measure,
  method : Method,
  ingredients: Ingredients,
  food_pairing: Array<string>,
  brewers_tips: string,
  contributed_by: string
}

export interface Measure{
  value : number,
  unit : string
}

export interface Method{
  mash_temp: [
    {
      temp : Measure,
      duration : number
    }
  ],
  fermentation: [
    {
      temp : Measure,
      duration? : number
    }
  ],
  twist?: any
}

export interface Ingredients{
  malt: [
    {
      name : string,
      amount : Measure
    }
  ],
  hops: [
    {
      name : string,
      amount : Measure,
      add : string,
      attribute : string
     }
  ],
  yeast : string
}
