import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Beer } from '../../shared/interfaces/Beer';
import { fromEvent } from 'rxjs';
import { filter, debounceTime, distinctUntilChanged, tap } from 'rxjs/operators';
import { SearcherService } from './services/searcher.service';

@Component({
  selector: 'app-searcher',
  templateUrl: './searcher.component.html',
  styleUrls: ['./searcher.component.scss']
})
export class SearcherComponent implements OnInit {

  public searchString: string = '';
  public loading: boolean = false;
  public results: Array<Beer> = [];
  public truncateSize: number = 250;

  @ViewChild('searchInput', {static: true}) searchInput: ElementRef;

  constructor(private searcherService: SearcherService) { }

  ngOnInit(): void {
    this.listenInput();
  }

  private getParams() : string
  {
    this.searchString = this.searchInput.nativeElement.value;
    let beer_name = this.searchString.trim().replace(/ /g, '_');
    return `food=${beer_name}`;
  }

  private listenInput(){
    fromEvent(this.searchInput.nativeElement, 'keyup')
      .pipe(
          filter(Boolean),
          debounceTime(330),
          distinctUntilChanged(),
          tap(() => {
            this.loading = true;
            this.searcherService.search(this.getParams()).subscribe((results) =>{
              this.results = results;

              /*Solo para simular condiciones de producción con delay de la API,
              se podría poner un interceptor y mostrar un componente en cada petición al servidor*/
              setTimeout(()=>{
                this.loading = false;
              },1300)
              /****************************************************************/
            });
          })
      ).subscribe();
  }



}
