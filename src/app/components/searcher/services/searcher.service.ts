import { Injectable } from '@angular/core';
import { ApiService } from '../../../services/api.service';
import { Observable } from 'rxjs/';
import { Beer } from '../../../shared/interfaces/Beer';

@Injectable({
  providedIn: 'root'
})
export class SearcherService {

  constructor(private apiService: ApiService)
  { }

  public search(params: string): Observable<Array<Beer>>{
    return this.apiService.get(`/beers?${params}`);
  }

  public random(): Observable<Beer>{
    return this.apiService.get('/beers/random');
  }

}
